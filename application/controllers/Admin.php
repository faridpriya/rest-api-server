<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Admin extends REST_Controller {
	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

	function index_get() {
        $id = $this->get('id_admin');
        if ($id == '') {
            $admin = $this->db->get('admin')->result();
        } else {
            $this->db->where('id_admin', $id);
            $admin = $this->db->get('admin')->result();
        }
        $this->response($admin, 200);
    }

    function index_post() {
        $data = array(
        	'nama_admin'  => $this->post('nama_admin'),
        	'username'  => $this->post('username'),
        	'password'  => $this->post('password'));
        $insert = $this->db->insert('admin', $data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    function index_put() {
        $where = array('id_admin'=>$this->put('id_admin'));
        $data = array(
        	'id_admin'   =>$this->put('id_admin'),
        	'nama_admin' => $this->put('nama_admin'),
        	'username' 	 => $this->put('username'),
        	'password'   => $this->put('password'));
        
        $update = $this->db->where($where)->update('admin', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    function index_delete()
	{
		$id_admin = $this->delete('id_admin');
		$this->db->where('id_admin',$id_admin);
		$delete = $this->db->delete('admin');
		if ($delete) {
			$this->response(array('status' => 'success'),201);
		}else{
			$this->response(array('status' => 'fail'),502);
		}
	}


}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */
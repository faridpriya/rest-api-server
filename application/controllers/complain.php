<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Complain extends REST_Controller {
	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

	function index_get() {
        $id = $this->get('id_complain');
        if ($id == '') {
            $complain = $this->db->get('complain')->result();
        } else {
            $this->db->where('id_complain', $id);
            $complain = $this->db->get('complain')->result();
        }
        $this->response($complain, 200);
    }

    function index_post() {
        $data = array(
        	'id_user'  => $this->post('id_user'),
        	'complain'  => $this->post('complain'),
        	'image'  => $this->post('image'),
        	'status'  => $this->post('status'),
        	'date'  => $this->post('date'));
        $insert = $this->db->insert('complain', $data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    function index_put() {
        $where = array('id_complain'=>$this->put('id_complain'));
        $data = array(
        	'id_complain' =>$this->put('id_complain'),
        	'id_user'  => $this->put('id_user'),
        	'complain'  => $this->put('complain'),
        	'image'  => $this->put('image'),
        	'status'  => $this->put('status'),
        	'date'  => $this->put('date'));
        
        $update = $this->db->where($where)->update('complain', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    function index_delete()
	{
		$id_complain = $this->delete('id_complain');
		$this->db->where('id_complain',$id_complain);
		$delete = $this->db->delete('complain');
		if ($delete) {
			$this->response(array('status' => 'success'),201);
		}else{
			$this->response(array('status' => 'fail'),502);
		}
	}

}

/* End of file complain.php */
/* Location: ./application/controllers/complain.php */
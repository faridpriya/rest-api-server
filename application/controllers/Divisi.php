<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Divisi extends REST_Controller {
	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

	function index_get() {
        $id = $this->get('id_divisi');
        if ($id == '') {
            $divisi = $this->db->get('divisi')->result();
        } else {
            $this->db->where('id_divisi', $id);
            $divisi = $this->db->get('divisi')->result();
        }
        $this->response($divisi, 200);
    }

    function index_post() {
        $data = array('nama_divisi'  => $this->post('nama_divisi'));
        $insert = $this->db->insert('divisi', $data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    function index_put() {
        $where = array('id_divisi'=>$this->put('id_divisi'));
        $data = array(
        	'id_divisi'=>$this->put('id_divisi'),
        	'nama_divisi'	=> $this->put('nama_divisi'));
        
        $update = $this->db->where($where)->update('divisi', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    function index_delete()
	{
		$id_divisi = $this->delete('id_divisi');
		$this->db->where('id_divisi',$id_divisi);
		$delete = $this->db->delete('divisi');
		if ($delete) {
			$this->response(array('status' => 'success'),201);
		}else{
			$this->response(array('status' => 'fail'),502);
		}
	}
}

/* End of file Divisi.php */
/* Location: ./application/controllers/Divisi.php */
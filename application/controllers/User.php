<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class User extends REST_Controller {
	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

	function index_get() {
        $id = $this->get('id_user');
        if ($id == '') {
            $user = $this->db->get('user')->result();
        } else {
            $this->db->where('id_user', $id);
            $user = $this->db->get('user')->result();
        }
        $this->response($user, 200);
    }

    function index_post() {
        $data = array(
        	'id_divisi'  => $this->post('id_divisi'),
        	'nama_user'  => $this->post('nama_user'),
        	'email'  => $this->post('email'),
        	'password'  => $this->post('nama_user'));
        $insert = $this->db->insert('user', $data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    function index_put() {
        $where = array('id_user'=>$this->put('id_user'));
        $data = array(
        	'id_user'=>$this->put('id_user'),
        	'id_divisi'	=> $this->put('id_divisi'),
        	'nama_user'	=> $this->put('nama_user'),
        	'email'	=> $this->put('email'),
        	'password'	=> $this->put('password'));
        
        $update = $this->db->where($where)->update('user', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    function index_delete()
	{
		$id_user = $this->delete('id_user');
		$this->db->where('id_user',$id_user);
		$delete = $this->db->delete('user');
		if ($delete) {
			$this->response(array('status' => 'success'),201);
		}else{
			$this->response(array('status' => 'fail'),502);
		}
	}

}

/* End of file User.php */
/* Location: ./application/controllers/User.php */